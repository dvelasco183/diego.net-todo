﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication9.Models;

namespace WebApplication9.Controllers
{
    public class Marca1Controller : Controller
    {
        private testEntities db = new testEntities();
        public ActionResult Index()
        {
            return View(db.MARCA.ToList());
        }



     
        public ActionResult Details(int? id)
        {
            if (id == null)
            {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            }
            MARCA marca = db.MARCA.Find(id);
            if (marca == null)
            {

                return HttpNotFound();

            }
            return View(marca);
        }

        //vista crear
        public ActionResult Create()
        {
            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IDmarca,descripcion")] MARCA marca)
        {
            if (ModelState.IsValid)
            {
                db.MARCA.Add(marca);
                db.SaveChanges();
                //volver al index
                return RedirectToAction("Index");
            }

            return View(marca);
        }

      
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MARCA marca = db.MARCA.Find(id);
            if (marca == null)
            {
                //no encuentra marca
                return HttpNotFound();
            }
            return View(marca);
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IDmarca,descripcion")] MARCA marca)
        {
            if (ModelState.IsValid)
            {
                db.Entry(marca).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(marca);
        }

    
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MARCA marca = db.MARCA.Find(id);
            if (marca == null)
            {
                return HttpNotFound();
            }
            return View(marca);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int  id)
        {
            MARCA mARCA = db.marca.Find(id);
            db.MARCA.Remove(marca);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
